* TODOs                                                                :RUST:
  :PROPERTIES:
  :CATEGORY: rust-lang
  :END:
** DONE bootstrap development environment
   SCHEDULED: <2020-05-26 Tue>
** DONE learn x in y minutes
   SCHEDULED: <2020-05-27 Wed>
** DONE hyperpolyglot
   SCHEDULED: <2020-05-27 Wed>
** exercism
** rust-lang book
** DONE cargo book
   SCHEDULED: <2020-06-30 Tue>
   - [X] getting started
   - [X] cargo guide
   - [X] cargo reference
   - [X] cargo commands
