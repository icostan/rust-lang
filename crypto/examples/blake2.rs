use blake2::{Blake2b, Digest};

fn main() {
    let hash = Blake2b::digest(b"my message");
    println!("Result: {:x}", hash);
}

#[test]
fn blake2_unit() {
    assert!(true);
}
