#[cfg(test)]
mod tests {
    #[test]
    fn tuples() {
        let _t = (1, "hello", true);
        let tup: (i32, &str, bool) = (1, "hello", true);

        // lookup
        let n: i32 = tup.0;
        assert_eq!(1, n);
        let s = tup.1;
        assert_eq!("hello", s);

        // deconstruct
        let (n, _s, _b) = tup;
        assert_eq!(1, n);
        let (n, _, _) = tup;
        assert_eq!(1, n);
    }
}
