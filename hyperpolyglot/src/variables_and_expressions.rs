#[cfg(test)]
mod tests {
    #[test]
    fn variables_and_expressions() {
        // write-once
        let _pi: f64 = 3.14;

        // mutable
        let mut n: i32 = 3;
        n += 1;
        assert_eq!(n, 4);

        // parallel assignments
        let (m, n) = (3, 7);
        assert_eq!(n, 7);
        assert_eq!(m, 3);

        // for string
        let mut s = "hello".to_string();
        s += " world";
        assert_eq!("hello world", s);

        // conditional expression
        let x = 0;
        if x > 0 {
            print!("{}", x);
        } else {
            print!("{}", -x);
        }

        // branch and mistmatch???
        if true {
            "hello";
        } else {
            3;
        }

        // null type, test
        // TODO: what is Option, Some?
        let a: Vec<Option<i32>> = vec![Some(3_i32), None, Some(-4_i32)];
        match a[0] {
            None => assert_eq!(a[0], None),
            Some(i) => assert_eq!(3, i),
        }
        if a[1] == None {
            assert!(a[1] == None);
        }

        // coalesce
        let a = vec![Some(1_i32), None];
        let _i: i32 = a[1].unwrap_or(0_i32);
    }
}
