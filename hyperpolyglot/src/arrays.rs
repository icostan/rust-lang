// use assert_panic::assert_panic;

#[cfg(test)]
mod tests {
    #[test]
    fn fixed_length() {
        let mut nums = [1i32, 2i32, 3i32];
        assert_eq!(3, nums.len());
        nums[0] = 4;
        assert_eq!(4, nums[0]);
    }
    #[test]
    fn resizable() {
        // TODO no type inference
        // let mut a = Vec::new();
        let mut a2: Vec<i32> = Vec::new();
        let a = vec![1, 2, 3];
        assert_eq!(3, a.len());

        // TODO assert panic?
        // assert_panic!(
        //     let n = a[7],
        //     "thread panics"
        // );

        // returns None:
        let n = a.get(7);
        assert_eq!(None, n);

        // manipulate
        a2.push(4);
        assert_eq!(1, a2.len());
        let _n = a2.pop();
        assert_eq!(0, a2.len());

        let a3 = a.clone();
        assert_eq!(3, a3.len());

        for i in a {
            println!("i: {}", i);
        }
        a2.reverse();
        a2.sort();
        if a2.contains(&7) {
            println!("contains 7");
        }
    }
}
