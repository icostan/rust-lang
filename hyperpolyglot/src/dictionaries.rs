#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    #[test]
    fn literal() {
        // TODO types are required
        // let mut dict = std::collections::HashMap::new();

        let mut dict: HashMap<&str, i32> = HashMap::new();
        dict.insert("t", 1);
        dict.insert("f", 0);
        assert_eq!(2, dict.len());
    }
}
