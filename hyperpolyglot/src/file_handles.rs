#[cfg(test)]
mod tests {
    // use std::io;

    #[test]
    fn test_name() {
        // standard handles
        // io::stdin;
        // io::stdout;
        // io::stderr;

        // read line
        // let s = io::stdin().read_line().ok().expect("Failed to read line");
        // assert!(s > 0);

        // print
        let s = "Hello, World!";
        println!("{}", s);
    }
}
