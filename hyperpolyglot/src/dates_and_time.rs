extern crate chrono;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn types() {
        // chrono::DateTime;

        let _dt = chrono::Local::now();
        let dt_utc = chrono::Utc::now();

        let t = dt_utc.timestamp();
        let _dt2 = chrono::NaiveDateTime::from_timestamp(t, 0);
    }

    #[test]
    fn format() {
        let dt = chrono::Local::now();
        dt.format("%Y-%m-%d %H:%M:%S");
    }
}
