#[cfg(test)]
mod tests {
    #[test]
    fn arithmetic_and_logic() {
        // add integer and float
        let n: i32 = 3;
        let x = n as f64;
        let y = x + 0.5;
        assert_eq!(y, 3.5);

        // power
        let _m = 2_i64.pow(32_u32);
        let _x1 = 3.14_f64.powi(32_i32);
        let _x2 = 3.14_f64.powf(3.5_f64);

        // sqrt
        let x = 16_f64.sqrt();
        assert_eq!(4_f64, x);
        let _x = (-1_f64).sqrt();
        // assert_eq!(None, x);
        let y = -1_f64.sqrt();
        assert_eq!(-1_f64, y);

        // transcedental functions
        let x = 0.5_f64;
        let mut _r = x.exp();
        _r = x.ln(); // x.log2() x.log10()
        _r = x.sin(); // x.cos() x.tan() x.asin() x.acos() x.atan()
        _r = x.atan2(3.1_f64);
        let _pi = std::f64::consts::PI;

        // float truncation
        let mut _r = 3.77_f64.trunc();
        _r = 3.77_f64.round();
        _r = 3.77_f64.floor();
        _r = 3.77_f64.ceil();

        // abs values
        let mut _r = -7_i32.abs();

        // random numbers
        let _x = rand::random::<f64>();

        // binary, octal, hex
        let _b = 0b101010;
        let _o = 0o52;
        let _h = 0x21;
    }
}
