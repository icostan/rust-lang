#[cfg(test)]
mod tests {
    #[test]
    fn test_name() {
        let result = std::panic::catch_unwind(|| {
            panic!("bam!");
        });
        assert!(!result.is_ok());
    }
}
