#[cfg(test)]
mod tests {
    fn add(x: f64, y: f64) -> f64 {
        x + y
    }

    fn add_one(x: f64) -> f64 {
        fn add(x1: f64, y1: f64) -> f64 {
            x1 + y1
        }

        add(x, 1.0)
    }

    #[test]
    fn functions() {
        let s = add(3.7, 2.8);
        assert_eq!(6.5, s);

        let s1 = add_one(3.7);
        assert_eq!(4.7, s1);
    }
}
