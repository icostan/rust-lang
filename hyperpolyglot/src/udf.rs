#[cfg(test)]
mod tests {
    #[allow(dead_code)]
    enum DayOfWeek {
        Mon,
        Tue,
        Wed,
        Thu,
        Fri,
        Sat,
        Sun,
    }

    #[test]
    fn user_defined_types() {
        let dow: DayOfWeek = DayOfWeek::Fri;

        // pattern match
        let msg = match dow {
            DayOfWeek::Fri => "Its Fri",
            _ => "Go to work...",
        };
        assert_eq!("Its Fri", msg);
    }
}
