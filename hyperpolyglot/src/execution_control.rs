#[cfg(test)]
mod tests {
    #[test]
    fn execution_control() {
        let signum: i32;

        // if
        let mut i: i32 = 1;
        if i > 0 {
            signum = 1
        } else if i == 0 {
            signum = 0
        } else {
            signum = -1
        }
        assert_eq!(1, signum);

        // while
        while i < 10 {
            i += 1
        }
        assert!(true);

        // for
        // TODO range keyword does not exist
        let mut n = 1;
        for i in 1..11 {
            n *= i;
        }
        assert!(n > 10);

        // loop
        loop {
            break;
        }
        assert!(true);
    }
}
