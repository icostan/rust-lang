#[cfg(test)]
mod tests {
    #[test]
    fn literals() {
        let s: &str = "don't say \"no\"";
        let s2: String = "don't say \"no\"".to_string();
        let s3 = "don't say \"no\"";
        assert_eq!(s, s2);
        assert_eq!(s, s3);
        assert!(s == s2);

        // TODO: \Uhhhhhhhh no longer supported
        let _s4 = "\0 \\ \t \n \r \" \\ \x7f \u{0a31}"; // \U83943432";
    }

    #[test]
    fn transform() {
        let s1: String = "hello".to_string();
        let s2: &str = " world";
        let s: String = s1 + s2;

        s.to_uppercase();

        let _n = "12".parse::<i32>().unwrap();
        let x = ".037".parse::<f64>().unwrap();
        assert_eq!(0.037, x);
    }
}
