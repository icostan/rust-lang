use std::io;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio_libtls::prelude::*;

async fn async_https_connect(servername: &str) -> io::Result<()> {
    let addr = &(servername.to_owned() + ":443");

    let request = format!(
        "GET / HTTP/1.1\r\n\
         Host: {}\r\n\
         Connection: close\r\n\r\n",
        servername
    );

    let config = Builder::new().build()?;
    let mut tls = connect(addr, &config, None).await?;
    tls.write_all(request.as_bytes()).await?;

    let mut buf = vec![0u8; 1024];
    tls.read_exact(&mut buf).await?;

    let ok = b"HTTP/1.1 200 OK\r\n";
    assert_eq!(&buf[..ok.len()], ok);

    Ok(())
}

#[tokio::main]
async fn main() {
    async_https_connect("www.example.com").await.unwrap();
}
