#[cfg(test)]
mod tests {
    #[test]
    #[allow(dead_code)]
    fn strcts() {
        struct Point {
            x: i32,
            y: i32,
        }
        let origin: Point = Point { x: 0, y: 0 };
        assert_eq!(origin.x, 0);
    }

    #[test]
    fn enums() {
        // Enum with fields
        #[allow(dead_code)]
        #[derive(Debug)]
        enum OptionalI32 {
            AnI32(i32),
            Nothing,
        }
        let _one: OptionalI32 = OptionalI32::AnI32(1);
        let two: OptionalI32 = OptionalI32::AnI32(2);
        match two {
            OptionalI32::AnI32(i) => assert_eq!(2, i),
            // OptionalI32::Nothing => assert!(false),
            _ => assert!(true),
        }
        // TODO how to check on enum's field
        // assert!(one == two);
    }

    struct Foo<T> {
        bar: T,
    }

    #[test]
    fn generics() {
        let b = Foo { bar: true };
        assert!(b.bar);
        let f = Foo { bar: 1.2 };
        assert_eq!(1.2, f.bar);

        // This is defined in the standard library as `Option`
        enum _Optional<T> {
            SomeVal(T),
            NoVal,
        }
        // assert!(Optional::SomeVal(true));
    }
    #[test]
    fn methods() {
        impl<T> Foo<T> {
            // Methods take an explicit `self` parameter
            fn bar(&self) -> &T {
                // self is borrowed
                &self.bar
            }
            fn bar_mut(&mut self) -> &mut T {
                // self is mutably borrowed
                &mut self.bar
            }
            fn into_bar(self) -> T {
                // here self is consumed
                self.bar
            }
        }

        let f = Foo { bar: 1.2 };
        // TODO self vs. &self?
        let e = 1.2;
        assert_eq!(&e, f.bar());
        assert_eq!(1.2, f.into_bar());

        let mut m = Foo { bar: 1.2 };
        println!("{}", m.bar_mut());
        // TODO what does &mut mean? no == defined?
        // assert_eq!(1.2, m.bar_mut());
    }

    #[test]
    fn traits() {
        trait Frobnicate<T> {
            fn frobnicate(self) -> Option<T>;
        }

        impl<T> Frobnicate<T> for Foo<T> {
            fn frobnicate(self) -> Option<T> {
                Some(self.bar)
            }
        }

        let another_foo = Foo { bar: 1 };
        assert_eq!(another_foo.frobnicate(), Some(1));
    }
}
