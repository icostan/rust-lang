#[cfg(test)]
mod tests {
    #[test]
    pub fn basics() {
        println!("{}", "basics.rs");

        let x: &str = "hello world";
        let y: String = "hello world".to_string();
        if x == y {
            println!("{}", "EQ");
        } else {
            println!("{}", "NOT EQ");
        }

        // Vectors/arrays //

        // A fixed-size array
        let four_ints: [i32; 4] = [1, 2, 3, 4];
        assert_eq!(3, four_ints[2]);

        // A dynamic array (vector)
        let mut vector: Vec<i32> = vec![1, 2, 3, 4];
        vector.push(5);
        assert_eq!(5, vector.len());
        assert_eq!(5, vector[vector.len() - 1]);

        // A slice – an immutable view into a vector or array
        // This is much like a string slice, but for vectors
        let _slice: &[i32] = &vector;
    }
}
