#[cfg(test)]
mod tests {
    #[test]
    fn test_name() {
        // Ranges
        for i in 0u32..10 {
            print!("{} ", i);
        }

        // `if` as expression
        let value = if true { "good" } else { "bad" };
        assert_eq!("good", value);
    }
}
