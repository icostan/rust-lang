* Rust-lang

** References
   1. [[https://www.rust-lang.org/learn/get-started]]
   2. [[https://doc.rust-lang.org/cargo/guide/index.html]]
   3. [[https://hyperpolyglot.org/rust]]
   4. [[https://learnxinyminutes.com/docs/rust/]]
   5. [[https://doc.rust-lang.org/book/title-page.html]]
   6. [[https://doc.rust-lang.org/stable/rust-by-example/]]
   7. [[https://exercism.io/my/tracks/rust]]

** Emacs integration
   - [X] major mode - rust-mode / rustic
   - [ ] snippets
   - [ ] auto completion
   - [ ] code navigation
   - [ ] documentation
   - [X] formatting
   - [ ] flycheck
   - [ ] linting
   - [ ] lsp
   - [ ] dap
